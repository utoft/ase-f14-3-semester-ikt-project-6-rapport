
Til kommunikationen mellem \devkit{} og \psoc{} er der implementeret et kernemodul til Linux kernen på \devkit{}.
Kernemodulet er implementeret som et "char device", hvilket betyder at data behandles som en "strøm af data".
Dette "char device" er konstrueret til at håndtere masterdelen af \protocol{} protokollen.
Det vil sige at det kun er masteren der kan starte en \protocol{} overførsel, og derfor anvendes dette modul ved hver overførsel.
Kernemodulet er ikke implementeret til \underline{kun} at agere kommunikationslinje mellem \devkit{} og \psoc{}, men også til at håndtere interrupts fra \protocol{} slaverne(\psoc{}).
Dette indebærer at hver gang der er sket en opdatering på en slave, og interruptlinjen sættes høj, er det kernemodulet der står for at læse hvilken adresse der er sket en ændring på, for derefter at vække den sovende applikation der er knyttet til den læste adresse.
Den værdi der returneres til applikationen er den opdaterede værdi.
\\
Den valgte metode til implementationen af kernemodulet giver en smule "policy" i kernen, da der tages et valg ud fra den læste adresse om hvilken sovende applikation der skal vækkes og derved modtage en opdateret værdi.
Valget kræver også at der implementeres mere end \underline{en} node pr. slave, og derved også flere tråde som skal læse fra disse noder.
Det er disse tråde(applikationer) der vækkes for efterfølgende at behandle den opdaterede værdi.
Hver enkelt node har derfor også en specifik funktionalitet som er beskrevet i \autoref{tab:kernel-nodes}.
Metoden er alligevel valgt på baggrund af beslutningen om at afskærme applikationerne på \devkit{} fra protokollen defineret i \autoref{tab:spi-protocol}.
På denne måde er det ikke nødvendigt for applikationerne at kende noget til protokollen, men kun hvilke noder der skal læses fra/skrives til for at opnå den ønskede funktionalitet.
Dette valg stemmer overens med valget om et eventbaseret applikationssystem.

For at lave et kernemodul med denne funktionalitet er det nødvendigt at implementere flere forskellige funktioner.
På \autoref{fig:devkit_kernel_init} ses et aktivitetsdiagram over \textit{init()} funktionen der kaldes når kernemodulet indsættes.

\begin{minipage}[t]{0.50\textwidth}
Ressourcer til de GPIOer der skal anvendes til interruptlinjerne requestes/anmodes.
GPIOer konfigureres til at være indgange.
Der anmodes om "threaded interrupts", hvilket anvendes i forbindelse med interrupts fra \psoc{}.
Det er nødvendigt at anvende "threaded interrupts" da det i "normale" interrupt service rutiner ikke er muligt at kalde en blokerende service.
Ved at anvende "threaded interrupts" kaldes der først en "hard isr", der fungerer på samme måde som en "normal" interrupt.
Hvis der i denne funktion returnes \textit{IRQ\_WAKE\_THREAD} så kaldes der en "soft isr" der afvikles en smule senere, men giver mulighed for blokerende funktionskald, hvilket er det der anvendes ved \protocol{}.
Derefter registreres \protocol{} driveren og major og minor numre allokeres.
Der er tilknyttet et major nummer til driveren, og sammen med dette et minor nummer til hver node.
Disse numre allokeres dynamisk og det er derfor ikke nødvendigt at vide hvilke numre der ikke er i brug på det specifikke system.							
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
	\begin{figure}[H]
\vspace{-17pt}
\centering
\includegraphics[width=0.85\textwidth,clip=true,trim=17 17 58 18]{../software/figures/activity_devkit_init.pdf}
\caption{Aktivitetsdiagram - init()}
\label{fig:devkit_kernel_init}
	\end{figure}
\end{minipage}

Det er ud fra minor numrene at der kan skelnes mellem hvilken node der er læst fra eller skrevet til.
For at oprette noderne skal der først oprettes en klasse.
Under denne klasse er det nu muligt at oprette de noder der skal anvendes til kommunikationen.
Til sidst tilføjes filoperationerne til systemet, og det er nu muligt at læse fra og skrive til noderne.

Kommunikationen til \protocol{} slaverne er ikke færdiginitialiseret endnu.
Slaverne skal først tilføjes til systemet.
Dette sker ved indsættelse af et ekstra kernemodul til hver slave.
Kernemodulet "simulerer" det koncept der hedder hotplug.
Hotplugging er når en enhed tilføjes til systemet mens det er kørende, og ikke kun når systemet starter op, hvilket kaldes coldplugging.
Hotplugging er som standard ikke understøttet af \protocol{} protokollen, men ved brug af denne metode kan det godt sammenlignes med det.
I hotplug kernemodulet listes de indstillinger som enheden er konfigureret til, og på denne måde er det muligt for kernemodulet at kommunikere korrekt med enheden.
De indstillinger der er specificeret i dette kernemodul er f.eks. CPHA, CPOL og overførselshastighed.

\begin{minipage}[t]{0.45\textwidth}
På \autoref{fig:devkit_kernel_read} ses et aktivitetsdiagram over \textit{read()} funktionen der kaldes hver gang der læses fra en node.
\\
Først læses der hvilken node \textit{read()} funktionen er blevet kaldt med.
Denne information kan uddrages fra minor nummeret.
Ud fra dette vælges hvilken adresse der ønsket læst fra.
Inden der læses fra adressen er det nødvendigt at kontrollere om noden er blevet åbnet som "blocking" eller "non-blocking".
Hvis den er åbnet med "non-blocking" læses der fra adressen direkte.
Hvis dette ikke er tilfældet lægges den kaldende applikation i søvn, og vækkes først igen når der er sket en ændring på den adresse der er knyttet til noden og derved minor nummeret.
Når applikationen vækkes læses den opdaterede værdi og returneres til userspace.
\\
\\
\\
På \autoref{fig:devkit_kernel_spi_read16} ses et aktivitetsdiagram over \textit{\protocol{}\_read16()} funktionen der anvendes hver gang der ønskes udført en læsning over \protocol{} interfacet.
Denne funktion anvendes både i \textit{read()} og \textit{soft\_isr()}.
Først kontrolleres det om der er indsat en \protocol{} enhed i form at et hotplug kernemodul.
Hvis dette ikke er tilfældet udføres der ingen læsning.
Hvis der er tilføjet en enhed låses en mutex.
Denne mutex sikrer at der ingen risiko er for at der prøves at sende data over \protocol{} interfacet på samme tid.
Når en mutex er låst falder den kaldende applikation i søvn indtil låsen frigives igen.
Derefter konstrueres en besked efter protokollen på \autoref{tab:spi-protocol} til en skrivning der bestemmer adressen der skal læses fra.
Meddelelsen tilføjes til \protocol{} sendebufferen og efter dette tilføjes et delay på \SI{150}{\micro\second}.
Dette er nødvendigt fordi \psoc{en} skal have lidt tid til at finde de data frem der skal læses.
Efterfølgende konfigureres bufferen til de data der læses og derefter udføres en \protocol{} synkronisering der sender og modtager de specificerede data.
Til sidst frigives den låste mutex så det er muligt at lave en \protocol{} overførsel igen.

\end{minipage}
\begin{minipage}[t]{0.55\textwidth}
	\begin{figure}[H]
\vspace{-17pt}
\centering
\includegraphics[height=0.45\textheight,clip=true,trim=17 17 30 18]{../software/figures/activity_devkit_read.pdf}
\caption{Aktivitetsdiagram - read()}
\label{fig:devkit_kernel_read}
	\end{figure}
	\begin{figure}[H]
%\vspace{-17pt}
\centering
\includegraphics[height=0.45\textheight,clip=true,trim=18 17 53 18]{../software/figures/activity_devkit_spi_read16.pdf}
\caption{Aktivitetsdiagram - \protocol{}\_read16()}
\label{fig:devkit_kernel_spi_read16}
	\end{figure}	
\end{minipage}

\begin{minipage}[t]{0.50\textwidth}
På \autoref{fig:devkit_kernel_hard_isr} ses et aktivitetsdiagram over \textit{hard\_isr()} funktionen der kaldes hver gang en af interruptlinjerne aktiveres af \protocol{} slaverne.
Først kontrolleres det om det er en applikation i kø.
Hvis dette ikke er tilfældet udføres der intet, og der returneres \textit{IRQ\_NONE}.
Hvis der er en applikation i kø er det nødvendigt at finde ud af hvilken \protocol{} slave der har aktiveret interruptet.
Ud fra dette, sættes der et flag.
Når flaget er sat, returneres \textit{IRQ\_WAKE\_THREAD}, hvilket betyder at \textit{soft\_isr()} funktionen kaldes, og i denne funktion evalueres flaget.
\\
\\
\\
\\
\\
På \autoref{fig:devkit_kernel_soft_isr} ses et aktivitetsdiagram over \textit{soft\_isr()} funktionen.
Først læses den adresse der er sket en ændring på fra den \psoc{} der aktiverede interrupt service rutinen, og derved også \textit{hard\_isr()} funktionen.
Dernæst kontrolleres det om den interne interrupt kø på \protocol{} slaven er tom.
Hvis dette er tilfældet sættes det flag der blev aktiveret i \textit{hard\_isr()} funktionen til nul, og dette betyder at der ikke er sket flere ændringer der skal håndteres.
Hvis køen ikke er tom, sættes der et flag som er knyttet til den læste adresse.
Dette flag evalueres når de applikationer der sover vækkes, og hvis dette flag er sandt lægges de ikke i søvn igen.
Efterfølgende sendes der et "ACK" til \protocol{} slaven, og den interne kø på slaven opdateres.
Nu vækkes alle de sovende applikationer, og hvis det flag der er knyttet til den enkelte ikke er sand, falder applikationen i søvn igen.
Hvis flaget er sandt, vækkes applikationen og den resterende del af \textit{read()} funktionen udføres som vist på \autoref{fig:devkit_kernel_read}.
Til sidst kontrolleres det om \textit{hard\_isr()} funktionen har været kaldt via en anden slave, mens den sekventielle gennemgang af \textit{soft\_isr()} blev udført. 
Hvis dette er tilfældet ændres den \psoc{} der læses fra næste gang.
Der skiftes herefter mellem de to slaver indtil begge interne køer er tomme.
Valget er taget for ikke at prioritere en af slaverne hvis der er sket ændringer på begge.

\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
	\begin{figure}[H]
\vspace{-17pt}
\centering
\includegraphics[width=0.95\textwidth,clip=true,trim=18 17 18 18]{../software/figures/activity_devkit_hard_isr.pdf}
\caption{Aktivitetsdiagram - hard\_isr()}
\label{fig:devkit_kernel_hard_isr}
	\end{figure}
	\begin{figure}[H]
\vspace{-17pt}
\centering
\includegraphics[width=0.95\textwidth,clip=true,trim=18 17 18 18]{../software/figures/activity_devkit_soft_isr.pdf}
\caption{Aktivitetsdiagram - soft\_isr()}
\label{fig:devkit_kernel_soft_isr}
	\end{figure}
\end{minipage}



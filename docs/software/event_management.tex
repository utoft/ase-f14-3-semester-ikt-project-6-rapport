%Event håndtering

\subsection{Design}

Event håndteringen er baseret på en blanding af Pub/Sub \citet{wiki:2014:pubsub} og observer \citet{wiki:2014:observer} mønstret.
Designet ligner mest Pub/Sub mønstret, men uden beskedkøer hvor beskedleveringen kører i en dedikeret tråd.
Event håndteringen er tilpasset fra \citet{kfriddile:2011:Online} med mange tilretninger.

Designet består af to komponenter.
En \textclass{Subscriber} klasse som bruges til at modtage events og en \textclass{Publisher} klasse som bruges til at sende events.

\subsubsection{Subscriber}
Subscriberen bruges til at subscribe til events de ønsker at modtage.
Tre informationer indgår i en subscription.

\begin{tabular}{ p{2cm}@{ \textbf{:} } p{0.8\textwidth} }
	\textbf{ID} & Besked ID.
	Struktureret som et filsystem.
	"/" betyder alle beskeder, "/psoc" betyder alle beskeder omhandlende \psoc{}en og "/psoc/servicemode" alle beskeder omhandlende psoc{}-servicemode.
	Valgfri, standard er "/" (matcher alle beskeder). \\
	\textbf{Type} & Typen er en beskedtype. Det kan være alle objekter som nedarver fra den generelle type \textclass{mediator::Message}. \\
	\textbf{Callback funktion} & En funktion som tager et ID og en besked som inputparametre. \\
\end{tabular}

Når der laves en \textclass{Subscription} fås et handle som skal gemme for at modtage events.
Slettes denne handle vil man ikke længere modtage events.

\subsubsection{Publisher}
Publisheren bruges til at sende et event til de Callback funktioner som matcher type og ID.

For at afsende et event kaldes funktionen Publish, som minimum kræver et ID eller en besked.
Sendes en besked uden ID er det kun modtager med IDet "/" som modtager beskeden.
Sendes en besked derimod kun med ID vil typen Signal automatisk vælges.
Det vil dog ofte være både ID og type der bruges til at sende en besked.

\subsection{Implementering}

Fordelen ved mediator implementeringen er at sender og modtager ikke kræver kendskab til hinanden.
Det er dog nødvendigt at vide under hvilket id og med hvilken type en besked afsendes.

En subscription indeholder 3 typer information ID, Type og Callback.

\textbf{ID}

ID et beskriver hvor eventen kommer fra.
Her er en liste eksempler af mulige IDer

\begin{itemize*}
\item /
\item /psoc
\item /psoc/servicemode
\item /psoc/servicemode/changed
\item /psoc/level/changed/1
\end{itemize*}

Når en komponent \textit{subscriber} til et event type er det ikke ligemeget om man beder om "/psoc/servicemode" eller "/psoc/servicemode/changed".
Er komponenten interesseret i beskeder med relation til \psoc{} servicemode kan man \textit{subscribe} til "/psoc/servicemode".
Derved får man beskeder af typen "/psoc/servicemode" og "/psoc/servicemode/changed".
Bindes der til "/" matches alle beskeder som sendes i systemet.

\textbf{Type}

Da et event kan indeholde information, er typen af besked vigtig.
Derfor matcher systemet også på type.
Sendes en besked som Message, men ønskes som Signal, så leveres beskeden ikke.

Den oprindelige kode som event håndteringen er skrevet ud fra var bygget på dynamisk kode der bruger C++ RTTI.
Det er dog slået fra i den kompiler som bruges til \devkit{} så derfor var en del ændringer nødvendige.

Problemet med typesammenligning er løst med denne kodestump.
\begin{figure}[H]
\begin{lstlisting}[language=C++]
class MakeTypeID
{
public:
	static int next_number() {
		static int magic(0);
		return magic++;
	}

	template <typename T_>
	static int number_for() {
		static int result(next_number());
		return result;
	}
};
\end{lstlisting}
\end{figure}
Det der er smart ved denne kode er at for hvert kald til \textfunction{MakeTypeID::number\_for<Type>()} returneres et unikt tal.
Dette sker fordi funktionen er statisk og indeholder en statisk variabel "result".
Når funktionen kaldes initialiseres dette tal ved at kalde \textfunction{MakeTypeID::next\_number()} ved følgende kald med samme type til \textfunction{MakeTypeID::number\_for<Type>()} vil det samme statiske resultat returneres.
Dette fungerer fordi funktionen er templatet og der derved genereres en ny funktion for hver type den kaldes med.

På den måde har det været muligt at sammenligne type IDer selvom det ikke direkte er muligt at bruge \textfunction{typeid()} og \textfunction{dynamic\_cast()}

\textbf{Callback}

Til Callback funktionen er \textfunction{boost::function} og \textfunction{boost::bind} blevet brugt for at forenkle bindingen til den funktion der skal modtage de pågældende events.

Det der gør kombinationen af \textfunction{boost::function} og \textfunction{boost::bind} smart er at \textfunction{boost::function} bruges til at lave et template for hvilke inputs er mulige at modtage.
\textclass{boost::bind} kan derved tage forskellige funktioner og få dem til at passe på kravet fra \textfunction{boost::function}.

I programmet er Callback funktion defineret således:

\begin{lstlisting}[language=C++]
//Callback
typedef boost::function< void( const MessageIdentifier& id, boost::shared_ptr<Message> message ) > Callback;

//beskrivelse
typedef boost::function< return( param1, param2 ) > Demo;
\end{lstlisting}

\textfunction{boost::function} er et template som tager et meget specifikt input.
Første værdi som står uden for parantesen er hvad funktionen skal returnere.
Værdierne inde i parantesen er de parametre funktionen skal tage.
Altså beskrivelsen af en helt normal C++ funktion.

Her er MessageIdentifier IDet, beskeden \textfunction{boost::shared\_ptr<Message>} og indholdet af beskeden Message Typen.
Callback funktionen returnerer altid beskedtypen "Message".
Det er modtagerens ansvar at caste det modtagede til den rigtige type.
Dette cast gøres ved brug af \textfunction{boost::static\_pointer\_cast< FaktiskType >(message)}

\textbf{boost::bind} er et template som gør det nemt at binde en funktion til en boost::function.
\begin{figure}[H]
\begin{lstlisting}[language=C++]
//Binding
boost::bind( &ControllerMain::handleServiceModeEnabled, this);
boost::bind( &ControllerMain::handlePaymentTotal, this, _2);
boost::bind( &DaemonAmountChanged::msgHandler, this, _1, _2);

//beskrivelse
boost::bind( &Class::methodToInvoke,classInstance);
\end{lstlisting}
\end{figure}

I ovenstående eksempel er to bindinger fra \textclass{ClassMain} og en fra \textclass{DaemonAmountChanged}.
Det smarte ved \textfunction{boost::bind} er at inputparameterene som \textfunction{boost::function} beskriver ikke nødvendigvis behøves være identisk med input af funktionen som skal kaldes.
Den første binding binder til en metode i en klasse som ikke har nogen inputparametre.

Den anden binding tager et input men det er ikke IDet som ellers er det første.
Den tager derimod \textfunction{boost::shared\_ptr<Message>} som første parameter.
Dette er ikke noget problem da \textfunction{boost::bind} ved hjælp af "\_2" kan specificere at anden input skal føres videre som første input.

Den tredje binding tager to inputs.
Disse inputs er identiske med input til \textfunction{boost::function}, derfor føres de to parametere videre med "\_1" og "\_2".

%\textbf{Besked}
%
%IDet beskriver hvad eventen kommer fra.
%Her er en eksempler af mulige IDer
%
%\begin{itemize*}
%\item /
%\item /psoc
%\item /psoc/servicemode
%\item /psoc/servicemode/changed
%\item /psoc/level/changed/1
%\end{itemize*}
%
%Når en komponent \textit{subscriber} til en event type er det ikke ligemeget om man beder om "/psoc/servicemode" eller "/psoc/servicemode/changed".
%Er komponenten interesseret i beskeder med relation til psoc servicemode kan man \textit{subscribe} til "/psoc/servicemode".
%Derved får man beskeder af typen "/psoc/servicemode" og "/psoc/servicemode/changed".
%Bindes der til "/" matches alle beskeder som sendes i systemet.

\clearpage
\subsubsection{SimpleSubscriber}

For at håndtere Subsciptions i programmet er der lavet en klasse \textclass{SimpleSubscriber} se \autoref{fig:simplesubscriber} der har funktionen \textfunction{Subscribe( const MessageIdentifier\& id, Handler callback )}.
Dette input gemmes i en klasse kaldet \textclass{Subscription} se \autoref{fig:subscription}.
Denne \textclass{Subscription} returneres til klassen i en \textclass{boost::shared\_ptr}.
En \textclass{boost::shared\_ptr} er en smart pointer.

\begin{figure}[H]
	\begin{center}
		\includeclassdiagram{\widthof{Mmediator::SimpleSubscriber}}{../software/figures/classmediator_1_1SimpleSubscriber__coll__graph}
	\end{center}
	\caption{Klasse - SimpleSubscriber}
	\label{fig:simplesubscriber}
\end{figure}


\clearpage
\textbf{Shared og weak pointer}

\begin{wrapfigure}{r}{\widthof{MMmediator::Subscription}}
	\begin{center}
		\includeclassdiagram{\widthof{Mmediator::Subscription}}{../software/figures/classmediator_1_1Subscription__inherit__graph}
	\end{center}
	\caption{Klasse - Subscription}
	\label{fig:subscription}
\end{wrapfigure}
Kort fortalt sørger den for at noget der er allokeret på heapen bliver slettet når der ikke længere er noget der refererer til det.

\textclass{SimpleSubscriber} gemmer også denne smart pointer som en weak pointer \textclass{boost::weak\_ptr}.
Forskellen mellem en \textclass{boost::shared\_ptr} og \textclass{boost::weak\_ptr} er at en weak pointer ikke tæller med som en der refererer til værdien.
Derfor kan værdien godt blive deallokeret selvom der er en weak pointer der stadig peger på værdien.

Begrundelsen for at bruge dette er hvis en \textclass{Subscription} som peger på en instans som er blevet deallokeret vil programmet crashe med en Segmentation Fault.
For at undgå dette bruges weak pointers.
Da de mister deres gyldighed hvis ejeren deallokeres.
Det sørger for at programmet kører stabilt.

\textbf{Liste over matchende subscriptions}

For at kunne sende beskeder skal det være muligt at få en liste over matchende \textclass{Subscription}s.
Dertil indeholder \textclass{SimpleSubscriber}, \textfunction{SubsciptionList, SimpleSubscriber::GetMatchingList( const MessageIdentifier\& id , const MessageType type)}.

Funktionen itererer listen m\_subscriptions og returnerer de \textclass{Subscription}s der matcher på både ID og Type.

\clearpage
\subsubsection{DirectPublisher}

Når en klasse skal sende et event til subscriberene i programmet bruges klassen \textclass{DirectPublisher} se \autoref{fig:directpublisher}.
Her kaldes funktionen \textfunction{void DirectPublisher::Publish<MsgType>( const MessageIdentifier\& id, boost::shared\_ptr<MsgType> message )} hvor MsgType er en template parameter.
Publish henter en liste af matchende \textclass{Subscription}'s via \textfunction{SubsciptionList::GetMatchingList(...)} og kalder Callback funktionen en efter en.

Dette betyder at det er den kaldende tråd som kommer til at udføre arbejdet.
Det er ikke normal praksis for et pub/sub system da det normalt bruger beskedkøer.
I dette system gøres det alligevel fordi mængden af arbejde der skal udføres ikke er tidskrævende.

\begin{figure}[H]
	\centering
	\includeclassdiagram{0.5\linewidth}{../software/figures/classmediator_1_1DirectPublisher__coll__graph}
	\caption{Klasse - DirectPublisher}
	\label{fig:directpublisher}
\end{figure}

For at være sikker på at event håndteringen virker, er det kritisk at lave tests.
\textclass{DirectPublisher} er testet med en testkode som gør følgende.

\begin{itemize*}
\item Opretter en \textclass{DirectPublisher}
\item Laver to subscriptions via \textclass{DirectPublisher::Subscribe}
	\begin{itemize*}
	\item ID: /test/message, Type: TextMessage, Callback: MessageHandler
	\item ID: /test/message/world, Type: TextMessage, Callback: AnotherMessageHandler
	\end{itemize*}
\item Sender 5 beskeder via \textclass{DirectPublisher::Publish}
	\begin{enumerate*}
	\item ID: /test/message/hello, Type: TextMessage, Text: "hello"
	\item ID: /test/message/world, Type: TextMessage, Text: "doesn't matter"
	\item ID: /test/message/, Type: AnotherTestMessage
	\item ID: /test/message/, Type: TextMessage, Text: "doesn't matter"
	\item ID: /foo/bar, Type: TextMessage, Text: "doesn't matter"
	\end{enumerate*}
\item Funktionen MessageHandler validerer Id om det er lig /test/message/hello og skriver så "Hello" til cout. AnotherTestMessage skriver " world" til cout når den kaldes.
\item Når programmet køres skal koden skrive "Hello world"
\end{itemize*}

Behandlingen af en event er beskrevet på \autoref{fig:eventhandling}.
Det vises også hvordan dette vises forsimplet.
Eventen /order/start sendes og behandles af to modtagere.

\begin{figure}[htbp]
  \centering
  \begin{sequencediagram}
      \newthread{send}{Event creator}
      \newinst{mh}{DirectPublisher}
      \newinst{rc1}{Receiver 1}
      \newinst{rc2}{Receiver 2}
    
    \begin{sdblock}{ Faktisk }{ Det egentlige eksekverings forløb}
		\begin{call}{send}{Publish(/order/start,...)}{mh}{}
			\begin{callself}{mh}{GetMatchingList(/order/start,...)}{}
			\end{callself}
			\begin{callself}{mh}{ForEach(...)}{}
				\begin{call}{mh}{Callback(/order/start,...)}{rc1}{}
				\end{call}
				\begin{call}{mh}{Callback(/order/start,...)}{rc2}{}
				\end{call}
			\end{callself}
		\end{call}
	\end{sdblock}
	
	\begin{sdblock}{ Forsimplet }{ Den forsimplede version }
	   \begin{call}{send}{/order/start}{rc1}{}
	   \end{call}
	   \begin{call}{send}{/order/start}{rc2}{}
	   \end{call}
   \end{sdblock}
  \end{sequencediagram}
  \caption{Softwaresekvens - Besked håndtering}
  \label{fig:eventhandling}
\end{figure}

\subsubsection{GraphPublisher}

Opslag i \textclass{DirectPublisher} er ikke særligt effektivt.
Den løber alle \textclass{Subscription}s igennem hver gang Publish kaldes.
Det skulle \textclass{GraphPublisher} løse da den var bygget på Grafteori\footnote{ Se \url{http://da.wikipedia.org/wiki/Grafteori} for mere information}.fxnote{lav ref} 

Grafen er implementeret som set på \autoref{fig:graphnode}.

\begin{figure}[H]
	\centering
	\begin{lstlisting}[language=C++]
	struct GraphNode {
		Hash ID;
		map< size_t, boost::shared_ptr< SubscriptionList > > members;
		map< Hash, boost::shared_ptr< GraphNode > > children;
		boost::weak_ptr< GraphNode > parent;
		MessageIdentifier getID();
	};
	\end{lstlisting}
	\caption{Kode - GraphNode}
	\label{fig:graphnode}
\end{figure}

For at forklare strukturen vises hvor en \textclass{Subscription} med ID "/psoc/servicemode" ville ligge i strukturen.
Et træ af \textclass{GraphPublisher} IDer ses på \autoref{fig:graphnodetree}.

\begin{figure}[H]
	\centering
	\begin{tikzpicture}
		\node (is-root) {0 (root)}
			[sibling distance=4cm]
			child { node {psoc}
				[sibling distance=25mm]
				child {node {servicemode}}
				child {node {canisteramount}}
			}
			child { node {order}
				[sibling distance=1cm]
				child {node {start}}
				child {node {done}}
			};
	\end{tikzpicture}
	\caption{GraphNode - Struktur}
	\label{fig:graphnodetree}
\end{figure}

Hver ID har en liste af \textclass{Subscription}s og \textclass{GraphNode}s.
Herunder er listen af \textclass{Subscription}s vist.

\begin{itemize*}
\item ID: 0 (root IDet bliver aldrig brugt)
	\begin{itemize*}
		\item Type: Message, Callback: Logger::handleLog
		\item Type: Signal, Callback: Logger::handleLog
		\item Type: GenericMessage<int>, Callback: Logger::handleLog
	\end{itemize*}
\item ID: psoc
\item ID: servicemode
	\begin{itemize*}
		\item Type: GenericMessage<int>, Callback: DaemonServiceMode::handleChanged
	\end{itemize*}
\end{itemize*}

Eksekveringsforløb.
\begin{enumerate*}
\item Denne struktur itereres ved at starte med rod noden.
\item \label{enum:searchid} Her søges i under IDer efter et ID matchende første del af MessageIdentifier. 
\item Findes IDet gennemgås dets medlemmer for matchende type og de tilføjes til retur listen.
\item Herefter returneres til step \ref{enum:searchid} for det fundne under ID.
\item Er alle hashes fra det ID der gennemgåes færdigt, returneres hele listen af matchende enheder.
\end{enumerate*}

Fordelen ved \textclass{GraphPublisher} skal være hurtigere eksekveringstid da implementeringer er mere kompleks end i \textclass{DirectPublisher}.

Efter en test blev det klart at indsættelse af nye \textclass{Subscription}s var noget dyrere tidsmæssigt og søgetid for at finde matchende \textclass{Subscription} var kun 3 til 5 procent hurtigere.
Derfor blev \textclass{GraphPublisher} skrottet, set i forhold til \textclass{DirectPublisher}.

\subsubsection{ServiceLoader}
\label{subsec:serviceloader}

Ved programinitialisering blev det hurtigt rodet med de mange \textclass{Subscription} der blev lavet i main.
For at undgå dette blev klassen \textclass{ServiceLoader} oprettet.
Målet med denne klasse er at sørge for at alle \textclass{Subscription}s er lavet inden den første besked bliver sendt.
Da nogle funktioner skal køre i deres egen tråd er programmet lavet således at trådene bliver oprettet i \textclass{ServiceLoader} hvis en klasse specificerer dette.
For at \textclass{ServiceLoader} kan kalde funktioner i de enkelte klasser skal alle klasser der loades nedarve fra \textclass{ServiceLoaderModes}.

\textclass{ServiceLoaderModes} har to funktioner.

\begin{itemize*}
	\item virtual subscribe()
	\item virtual run()
\end{itemize*}

\textfunction{ServiceLoaderModes::subscribe()} kaldes med en \textclass{Subscriber} instans som input parameter.
I denne funktion skal klassen oprette de \textclass{Subscription}s der skal anvendes i resten af programmet.

\textfunction{ServiceLoaderModes::run()} kaldes med en \textclass{Publisher} instans som input parameter.
I denne funktion kan en klasse kører opstartskode.
Hvis klassen har brug for at køre i sin egen tråd som en service, er det denne funktion som køres i en ny tråd.

Begrundelsen for at \textclass{Publisher} og \textclass{Subscriber} anvendes som parametre i disse funktioner er for at opfordre udvikleren til at udføre \textclass{Subscription}s og opstartskode på en ensartet facon.

\textclass{ServiceLoader} har to funktioner.

\begin{itemize*}
	\item addClass()
	\item exec()
\end{itemize*}

\textfunction{ServiceLoader::addClass()} tilføjer en klasse som nedarver fra  \textclass{ServiceLoaderModes} til en liste over klasser der skal loades.

\textfunction{ServiceLoader::exec()} gennemløber først alle tilføjede klasser og kalder funktionen \textfunction{ServiceLoaderModes::subscribe()}.
Når dette er udført løbes listen igennem igen hvor \textfunction{ServiceLoaderModes::run()} kaldes. Kaldet kan ske i en dedikeret tråd hvis klassen ønsker det.

På denne måde er risikoen for at beskeder skal gå tabt minimal, da ingen tråde begynder at sende beskeder før at alle klasser modtager disse events.
